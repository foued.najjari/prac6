# Deploying to AWS ECS Example

### Project variables

This project needs to have these project variables defined. They are:

* `AUTO_DEVOPS_PLATFORM_TARGET` - If the ECS service you're deploying to is EC2-based (EC2 launch type) then set this variable to `ECS`. If the ECS service you're deploying to is fargate-based (fargate launch type) then set this variable to `FARGATE`
* `AWS_ACCESS_KEY_ID` - `AWS access key` found in your local directory $HOME/.aws/credentials (to set up, please refer to AWS documentation)
* `AWS_SECRET_ACCESS_KEY` - `AWS secret key` found in your local directory $HOME/.aws/credentials (to set up, please refer to AWS documentation)
* `AWS_REGION` - AWS region where your ECS cluster is running
* `CI_AWS_ECS_CLUSTER` - this is the name of the ECS cluster you have created on AWS
* `CI_AWS_ECS_SERVICE` - this is the name of the ECS service (in your ECS cluster) you will deploy your application to
* `CI_AWS_ECS_TASK_DEFINITION` - this is the name of the task definition File name that your ECS service will use. A task definition, which defines the characteristics of your containers, is required to run Docker containers in Amazon ECS. You can define multiple containers in a task definition. The parameters that you use depend on the launch type you choose for the task.
* `CI_AWS_ECS_TASK_DEFINITION_FILE` - either the location OR the content of your locally-defined task definition file. If the variable contains the location of your task definition file within your GitLab project then set the Variable Type to `Variable`. If you paste the content of your task definition file into the `Value` text field on this variable, then set the Variable Type to `File`.
* `TEST_DISABLED` - optional variable. You can skip defining this variable. Set it to `true` if you would like Auto DevOps to skip the running of all tests. Your demo will run faster if you set this variable.

### How to exercise this demo

1. Create an ECS Cluster on AWS

    This is the sequence that I followed to create an ECS Cluster on AWS:

    1.1. Create a task definition file on Amazon ECS console. You can define it to be either EC2-based or fargate-based.

    1.2. Create an ECS Cluster on Amazon ECS console

    1.3. Create an Application Load Balancer (ALB) on Amazon EC2 Console. If you plan on using a fargate-based ECS service, then you can skip this step.

    1.4. Create an ECS Service in the previously created ECS Cluster via the Amazon ECS console. Ensure that your ECS Service uses the task definition file you created in step 1.1. If defining a fargate-based ECS service, then ensure to set `Auto-assign public IP` to `ENABLED`.

    1.5 If your ECS Cluster has only EC2-based services, then you will need to adjust the security group of your EC2 instance of your ECS Cluster to accept traffic from the ALB you created in step 1.3. You can skip this step if your ECS Cluster only has fargate-based services.

2. Set the project variables to your respective values on your AWS ECS Cluster
3. Change the color of the background in the following two project source files: `src/main/java/com/example/demo/DemoApplication.java` and `src/test/java/com/example/demo/DemoApplicationTests.java`. This will trigger a build in the main branch that will deploy your ECS service to your ECS cluster using your locally defined task definition file.


### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
